# Netflix - Technical Solutions Developer

## Questions

1 - Explain the difference between an Abstract Class and Interface? When would you use
one over the other?

An Abstract Class is a class that cannot be instantiated but can be subclassed. It may or may not include abstract methods.
A class can extend only one abstract class.

An Interface is a type that also cannot be instantiated, but cannot be subclassed, it can only be extended by other interfaces and implemented by classes. A class can implement one or more interfaces.

Both features can run into issues related to multiple inheritance (Diamond Problem).

Since Java 8, Interfaces can also contain default methods.

You want to use Interfaces when you want to define functionality, but not how it is implemented. You're not actually concerned about sharing implementations.

You want to use Abstract Classes when you want to share implementations between classes. Abstract classes can also access it's implementation state. 

Personal Opinion: I tend to prefer Interfaces over Abstract Classes as I find them dangerous most of the time.

2 - Explain what generics in Java are and what are the benefits.

Generic Types are a feature introduced in Java 5 that enables you to implement code that can handle more complex data type combinations, as it allows a type or method to operate on more complex objects while still promising compile-time type safety.

One of it's benefits is that it allows you to write generic implementations of specific algorithms, such as a List, Set or a Map, and allow them to work with different types according to their needs. The [List](https://docs.oracle.com/javase/8/docs/api/java/util/List.html) Interface makes use of Generic Types to allow it's usage with any kind of data type, for example.

The same concept can be found in other typed languages such as [Haskell](https://wiki.haskell.org/Generics) and [Typescript](https://www.typescriptlang.org/docs/handbook/generics.html)

However, Java Generics can also be dangerous, as their usage together with the usage of Wildcards can also prove that Java Type System can be unsound, as (this paper)[https://raw.githubusercontent.com/namin/unsound/master/doc/unsound-oopsla16.pdf] explains.

## Programming Test

The application uses `gradle` as a task runner, so most management
can be done using it.

The process expects only one argument, which is the json input file path.

If it is a file that doesn't exists, the application will just do nothing.

If it is a file that does exists but is not a valid json, the application will throw a `JsonParsingException`.

If it is a file that does exists, a valid json, but don't follow the 
expected structure, it will either return an empty object, or an Exception with the stack trace explaining why it failed.

Check unit tests for more description.

### Building the application

`gradle build`

### Running the application

`gradle run --args="/path/to/json/input.json"`

Example:

`gradle run --args="src/test/resources/inputs/expected-structure.json"`

### Running unit tests

`gradle test`
