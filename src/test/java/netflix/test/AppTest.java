package netflix.test;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class AppTest {
    public Optional<String> getJsonContent(String resourceName) {
        final ClassLoader classLoader = getClass().getClassLoader();
        
        try {
            final FileInputStream inputStream = new FileInputStream(classLoader.getResource(resourceName).getFile());
            final JsonReader jsonReader = Json.createReader(inputStream);
            final JsonObject jsonObject = jsonReader.readObject();

            return Optional.of(jsonObject.toString());
        } catch (IOException exception) {
            return Optional.empty();
        }
    }

    @Test public void testPersonnelConsolidatorExpectedStructure() {
        final App classUnderTest = new App();
        final String input = this.getJsonContent("inputs/expected-structure.json").get();
        final String output = this.getJsonContent("outputs/expected-structure.json").get();
        final String result = classUnderTest.consolidatePersonnel(input);

        assertEquals("app should consolidate personnel with expected structure", output, result);
    }

    @Test public void testPersonnelConsolidatorEmptyStructure() {
        final App classUnderTest = new App();
        final String input = this.getJsonContent("inputs/empty-structure.json").get();
        final String output = this.getJsonContent("outputs/empty-structure.json").get();
        final String result = classUnderTest.consolidatePersonnel(input);

        assertEquals("app should consolidate personnel with empty structure", output, result);
    }

    @Test public void testPersonnelConsolidatorInvalidStructure() {
        final App classUnderTest = new App();
        final String input = this.getJsonContent("inputs/invalid-structure.json").get();
        final String output = this.getJsonContent("outputs/empty-structure.json").get();
        final String result = classUnderTest.consolidatePersonnel(input);

        assertEquals("app should consolidate personnel with invalid structure", output, result);
    }

     @Test public void testPersonnelConsolidatorInvalidValues() {
        final App classUnderTest = new App();
        final String input = this.getJsonContent("inputs/invalid-values.json").get();
        final String output = this.getJsonContent("outputs/empty-structure.json").get();
        final String result = classUnderTest.consolidatePersonnel(input);

        assertEquals("app should consolidate personnel with invalid values", output, result);
    }

    @Test(expected = Exception.class)
    public void testPersonnelConsolidatorInvalidArrayValues() {
        final App classUnderTest = new App();
        final String input = this.getJsonContent("inputs/invalid-array-values.json").get();

        classUnderTest.consolidatePersonnel(input);
    }
}
