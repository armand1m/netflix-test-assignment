package netflix.test;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.json.*;

public class App {
    public String consolidatePersonnel(String json) {
        final JsonObject object = Json
            .createReader(new StringReader(json))
            .readObject();

        final InputPayload payload = new InputPayload(object);

        final Map<String, Object> consolidated = payload.roles
            .stream()
            .collect(Collectors.toMap(
                role -> role.name,
                role -> payload.personnel
                    .stream()
                    .filter(person -> person.roleID == role.id)
                    .map(person -> person.name)
                    .collect(Collectors.joining(", "))
            ));

        return Json.createObjectBuilder(consolidated).build().toString();
    }

    public Optional<String> getInput(String filePath) {
        try {
            return Optional.of(new String(Files.readAllBytes(Paths.get(filePath))));
        } catch (IOException exception) {
            return Optional.empty();
        }
    }

    public static void main(String[] args) {
        final App app = new App();
        final Optional<String> input = app.getInput(args[0]);

        input.ifPresent((value) -> System.out.println(app.consolidatePersonnel(value)));
    }
}
