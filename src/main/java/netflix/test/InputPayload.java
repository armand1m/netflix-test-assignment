package netflix.test;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;
import javax.json.JsonValue.ValueType;

public class InputPayload {
    public List<Person> personnel = new ArrayList<Person>();
    public List<Role> roles = new ArrayList<Role>();

    public InputPayload(JsonObject object) {
        if (this.hasValidKeys(object) && this.hasValidValueTypes(object)) {
            this.personnel = object.getJsonArray("personnel").getValuesAs(value -> {
                try {
                    return new Person(value.asJsonObject());
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            });

            this.roles = object.getJsonArray("roles").getValuesAs(value -> {
                try {
                    return new Role(value.asJsonObject());
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            });
        }
    }

    private Boolean hasValidKeys(JsonObject object) {
        return object.containsKey("personnel")
            && object.containsKey("roles");
    }

    private Boolean hasValidValueTypes(JsonObject object) {
        final Boolean hasPersonnelArray = object.getValue("/personnel").getValueType() == ValueType.ARRAY;
        final Boolean hasRolesArray = object.getValue("/roles").getValueType() == ValueType.ARRAY; 

        return hasPersonnelArray && hasRolesArray;
    }
}