package netflix.test;

import javax.json.JsonObject;
import javax.json.JsonValue.ValueType;

public class Role {
    public Integer id;
    public String name;

    public Role(JsonObject jsonObject) throws Exception {
        if (!this.isValidJsonObject(jsonObject)) {
            throw new Exception("Invalid Role Json Structure.");
        }

        this.id = jsonObject.getInt("id");
        this.name = jsonObject.getString("name");
    }

    private Boolean isValidJsonObject(JsonObject object) {
        final Boolean containKeys = object.containsKey("name")
            && object.containsKey("id");

        if (!containKeys) return false;

        final Boolean containValidNameType = object
            .getValue("/name")
            .getValueType() == ValueType.STRING;
        
        final Boolean containValidRoleIdType = object
            .getValue("/id")
            .getValueType() == ValueType.NUMBER;

        return containValidNameType && containValidRoleIdType;
    }
}
