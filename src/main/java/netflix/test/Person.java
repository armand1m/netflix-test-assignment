package netflix.test;

import javax.json.JsonObject;
import javax.json.JsonValue.ValueType;

public class Person {
    public Integer roleID;
    public String name;

    public Person(JsonObject jsonObject) throws Exception {
        if (!this.isValidJsonObject(jsonObject)) {
            throw new Exception("Invalid Person Json Structure.");
        }
        
        this.name = jsonObject.getString("name");
        this.roleID = jsonObject.getInt("roleID");
    }

    private Boolean isValidJsonObject(JsonObject object) {
        final Boolean containKeys = object.containsKey("name")
            && object.containsKey("roleID");

        if (!containKeys) return false;

        final Boolean containValidNameType = object
            .getValue("/name")
            .getValueType() == ValueType.STRING;
        
        final Boolean containValidRoleIdType = object
            .getValue("/roleID")
            .getValueType() == ValueType.NUMBER;

        return containValidNameType && containValidRoleIdType;
    }
}
